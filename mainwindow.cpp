#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_runButton_clicked()
{
    const QString program = "ffmpeg";
    QStringList arguments = ui->inputLine->text().split(QRegExp("\\s+"), QString::SkipEmptyParts);
    QProcess *ffmpeg = new QProcess();
    ffmpeg->start(program, arguments);
    ffmpeg->waitForFinished();
    QByteArray output = ffmpeg->readAllStandardOutput();
    QMessageBox::information(this,
			     tr("Program Output"),
			     tr(output));
}
