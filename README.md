This is Gegen, an experimental cross-platform media conversion and streaming tool.

Gegen needs FFmpeg and Qt 5 to function properly.

The name "Gegen" derives from the [Gegenees](https://en.wikipedia.org/wiki/Gegenees), a race of six-armed giants in Greek mythology.

Gegen is free software, released under the terms of the GNU GPL v3 or any later version.
